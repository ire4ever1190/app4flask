FROM python:3.6-alpine
RUN touch requirements.installed
ARG url
ENV loginUrl=url
RUN apk update
RUN apk add gcc libxml2-dev python3-dev libxslt-dev musl-dev
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY app4flask app4flask
WORKDIR app4flask
RUN python createDB.py
EXPOSE 80
CMD ["gunicorn", "-w", "1","-b", "0.0.0.0:81", "app:app"]