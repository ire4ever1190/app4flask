# app4flask
[![pipeline status](https://gitlab.com/ire4ever1190/app4flask/badges/master/pipeline.svg)](https://gitlab.com/ire4ever1190/app4flask/commits/master)
This is a simple flask server to scrape your timetable from app4 website then display it has a simple to parse webpage so that it can be 
displayed in things such has an web app or in things such has conky/rainmeter

To run first clone this then cd into the dir then run.
```
sudo pip install -r requirements.txt
```
make a file called database.db and run
```
python createDB.py
```
in the app4flask dir so that the database file is created

Then set your schools name from the command line.
You find the school name by looking at the app4 url
https://{ This is your school name}.app4.ws
```
export loginUrl={{ first login url  }}
```

Then to run the server first cd into 'app4flask' then run.
```
python app.py
```
go to http://127.0.0.1:5000/ and login using you SIMON school details

If you want to host it, you need to set up a uWSGI/nginx config or use gunicorn.


### Development

To use the tests you need to run 
```
pip install -r requirements.txt
pip install pytest
```
Then to run tests you run
```
cd app4flask
pytest
```
