


function sendhttp(url){
    // Creates the request object
    var xhttp = new XMLHttpRequest();
	xhttp.open("get", url);
	// Sets the timezone headers
	// Sends the timezone that the persons device is set to (usually is automatically set)
	xhttp.setRequestHeader("timezone", Intl.DateTimeFormat().resolvedOptions().timeZone);
	xhttp.withCredentials = true;
	xhttp.send();
	// Runs this once the request is finished
	xhttp.onload = function listener() {
	        console.log(xhttp.response);
	        var response = JSON.parse(xhttp.response);
            var session;
            // Finds all the HTML tags
            var classes = document.getElementsByTagName("Class");
            var times = document.getElementsByTagName("Time");
            var teachers = document.getElementsByTagName("Teacher");
            var rooms = document.getElementsByTagName("Room");
            var day = document.getElementById("day");
            // Sets the hidden day tag to the day of the timetable so that the next day and previous day numbers can
            // be found
            day.innerHTML = response[0]['day'];
            // Loops over the Sessions and writes the info to the document
            for (session = 1; session < 10; session++) {
                var x = session - 1;
                classes[x].innerHTML = response[0]['session' + session]['Class'];
                rooms[x].innerHTML = response[0]['session' + session]['Room'];
                times[x].innerHTML = response[0]['session' + session]['Time'];
                teachers[x].innerHTML = response[0]['session' + session]['Teacher'];


            }
            console.log("selected day is ");
            console.log(day.innerHTML);

    }
    }


// Gets the next days timetable relative to the day already displayed
function nextday() {
    var day = document.getElementById("day").innerHTML;
    var newday = parseInt(day) + 1;
            if (newday > 5){
            newday = 0;
            document.getElementById("day").innerHTML = 0
        }
    sendhttp("/list/" + newday);
}
// Gets the previous days timetable relative to the day already displayed
function prevday() {
        var day = document.getElementById("day").innerHTML;
        var newday = parseInt(day) - 1;
        if (newday < 0){
            newday = 4;
            document.getElementById("day").innerHTML = 4
        }
        sendhttp("/list/" + newday);
}
// Runs this once the page is fully loaded
window.onload = sendhttp("/list");

