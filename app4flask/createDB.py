import sqlite3

db = sqlite3.connect('database.db')


def createTableUsers():
	db.execute("""
	create table tblUsers
	(
	  userID       VarChar(5)
	    constraint tblUsers_pk
	      primary key,
      	  userPassword varchar,
	  userGuid     VarChar
	);

	""")


def createTableSessions():
	db.execute("""create table tblSession
	(
	  userID references tblUsers (userID),
	  sessionDay   VarChar,
	  sessionID    VarChar,
	  sessionClass VarChar,
	  sessionRoom  VarChar,
	  sessionTime  VarChar,
	  sessionTeacher VarChar,
	  PRIMARY KEY (userID, sessionDay, sessionID)

	);""")


def createTableCookies():
	db.execute("""create table tblCookies (
			userID references tblUsers (userID), 
			cookieSession VarChar, 
			cookieSimonLogin VarChar, 
			cookieApi VarChar,
			PRIMARY KEY (userID)
		)""")

def createDB():
	try:
		createTableUsers()
	except:
		db.execute("""DROP TABLE tblUsers""")
		createTableUsers()

	try:
		createTableSessions()
	except:
		db.execute("""DROP TABLE tblSession""")
		createTableSessions()

	try:
		createTableCookies()
	except:
		db.execute("""DROP TABLE tblCookies""")
		createTableCookies()
if __name__ == '__main__':
	createDB()