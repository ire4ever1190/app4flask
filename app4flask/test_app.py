import requests
import os
import mechanicalsoup
from createDB import *
from datahandler import *
createDB()
username = str(os.environ["username"])
password = str(os.environ["password"])
session = requests.session()
student = Student(username, password)
def browser_base_login():
	"""
	The basic way of writing the credentials to the form
	:return:
		A browser object that has the username and password entered into the login form
	"""
	s = requests.Session()
	br = mechanicalsoup.StatefulBrowser(session=s)
	br.open("http://127.0.0.1:5000/login")
	br.select_form(nr=0)
	br["username"] = username
	br["password"] = password
	return br

def check_for_nothing():
	for day in range(0, 5):
		response = session.post(f'http://127.0.0.1:5000/list/{day}')
		for Session in response.json():
			for key in Session:
				assert "Nothing" != Session.get(key)


class TestApp(object):

	def test_browser_normal(self):
		"""Tests just logging onto the timetable"""
		br = browser_base_login()
		br.submit_selected()
		assert br.get_current_page().find('title').text.strip() == f"{username}'s Timetable"

	def test_browser_remember_me_cookies_check(self):
		"""Tests if the the user gets the studentNum cookie"""
		br = browser_base_login()
		br["remember"] = "y"
		br.submit_selected()
		for cookie in br.get_cookiejar():
			if cookie.name == 'sessionCookie':
				assert student.check_session_cookie(cookie.value)

	def test_browser_update(self):
		"""Tests if the update button works with login"""
		br = browser_base_login()
		br["update"] = "y"
		br.submit_selected()
		for cookie in br.get_cookiejar(): session.cookies.set(cookie.name, cookie.value)
		check_for_nothing()

	def test_browser_update_cookies(self):
		"""Tests if the update and remember me button works with login"""
		br = browser_base_login()
		br["update"] = "y"
		br["remember"] = "y"
		br.submit_selected()
		br.open("http://127.0.0.1:5000/timetable")
		assert br.get_current_page().find('title').text.strip() == f"{username}'s Timetable"
		for cookie in br.get_cookiejar(): session.cookies.set(cookie.name, cookie.value)
		check_for_nothing()

	def test_browser_remember_me_cookies_login(self):
		"""Checks if the user is remembered and automatically logged in"""
		br = browser_base_login()
		br["remember"] = "y"
		br.submit_selected()
		br.open("http://127.0.0.1:5000/timetable")
		assert br.get_current_page().find('title').text.strip() == f"{username}'s Timetable"

