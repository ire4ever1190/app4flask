import json
import os
import re
import secrets
import sqlite3
from datetime import datetime, timedelta
from logger import Logging
from pprint import pformat

import mechanicalsoup
import requests
from passlib.hash import pbkdf2_sha256

log = Logging()

try:
	check_password_env = os.environ['check_password']
	if check_password_env == 'False':
		check_password = False
	else:
		check_password = True
except KeyError:
	check_password = True


class Student:
	def __init__(self, username, password=None):
		self.username = username
		self.password = password
		self.session = requests.session()
		self.conn = sqlite3.connect('database.db')
		self.db = self.conn.cursor()
		self.login_url = str(os.environ["loginUrl"])
		self.base_url = re.search('https?://.*/', self.login_url).group(0)

	def login(self):
		"""
		logins to SIMON
		:return:
			mechanicalsoup.StatefulBrowser object that is logged in
		"""
		assert self.password is not None, 'login function needs password'
		log.debug(f"logging in with user {self.username}")
		br = mechanicalsoup.StatefulBrowser(
			user_agent="Timetable scraping Bot: https://gitlab.com/ire4ever1190/app4flask"
		)
		br.set_verbose(2)
		br.open(self.login_url)
		br.select_form(nr=0)
		br['username'] = self.username
		br['password'] = self.password
		br['trusted'] = "4"
		br.submit_selected()
		br.select_form(nr=0)
		br['inputUsername'] = self.username
		br['inputPassword'] = self.password
		br['inputRememberMe'] = "remember-me"
		br.submit_selected()
		self.session = br.session
		return br


	def request_info(self, info="default", require_cookies=False, url=None, **kwargs):
		url = f"{self.base_url}Default.asmx/{info}" if url is None else url
		if require_cookies:
			self.check_cookies()
		headers = {
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0',
			'Accept': 'application/json, text/javascript, */*; q=0.01',
			'Accept-Language': 'en-GB,en;q=0.5',
			'Referer': 'https://intranet.stpats.vic.edu.au/',
			'Content-Type': 'application/json; charset=utf-8',
			'X-Requested-With': 'XMLHttpRequest',
			'DNT': '1',
			'Connection': 'keep-alive',
		}
		# Gets the time since epoch in milliseconds has a string
		self.get_cookies()
		self.session.cookies.set("logondata", f"acc=0&lgn={self.username}")
		params = ((str(int(datetime.now().timestamp() * 1000)), ''),)
		response = self.session.post(url, params=params, headers=headers, **kwargs)
		log.request(pformat(response.json()))
		return response


	def add_user(self):
		"""
		adds the users username, password hash, guid and SIMON auth cookies into the database
		:return:
			Nothing
		"""

		try:
			self.get_cookies(bake_fresh=True)
			self.db.execute(f"""INSERT INTO tblUsers (userID, userPassword, userGuid) VALUES ('{str(
				self.username)}', '{pbkdf2_sha256.hash(self.password)}', '{self.get_guid()}')""")
			self.add_session_cookie()
		except sqlite3.IntegrityError as e:
			print('already in database')
			pass
		else:
			self.conn.commit()


	def add_session_cookie(self):
		"""
		Makes a session cookie and adds it value into the database so that it can be checked
		:return:
			The value of the session cookie
		"""
		sessionCookie = secrets.token_urlsafe(20) + self.username
		self.db.execute(
			f"""UPDATE tblCookies set  cookieSession = '{sessionCookie}' WHERE userID is '{self.username}'""")
		self.conn.commit()
		return sessionCookie


	def add_data(self):
		"""
		Adds the users timetable into the database
		:return:
			Nothing
		"""
		self.add_cookies(update_cookies=True)
		self.add_timetable_for_week()


	def add_timetable_for_week(self):
		"""
		Adds the users timetable for the week into the database
		and if it is already in the database then it updates it
		:return:
			Nothing
		"""
		self.check_cookies()
		today = datetime.now().date()
		week_start = today - timedelta(days=today.weekday())
		for day in range(5):
			selected_day = week_start + timedelta(days=day)
			self.add_timetable_for_day(day=selected_day)


	def add_timetable_for_day(self, day=datetime.now()):
		"""
		Adds the timetable for the current day
		:return:
			Nothing
		"""
		timetableJson = self.get_timetable(day.isoformat())
		for sessionID, period in enumerate(timetableJson['d']['Periods']):
			sessionID += 1
			class_time = period['StartTime'] + ' - ' + period['EndTime']
			session = period['Classes'][0]
			try:
				self.db.execute(f"""INSERT INTO tblSession  VALUES (
						"{self.username}",
						"{str(day.weekday())}",
						"{str(sessionID)}",
						"{session['Description']}",
						"{session['Room']}",
						"{class_time}",
						"{session['TeacherName']}")""")
				print('inserted a session')
			# Updates if there is already timetable info in the database
			except:
				self.db.execute(f"""
							UPDATE tblSession Set sessionClass = '{session['Description']}',
									      sessionClass = '{session['Room']}', 
									      sessionTime = '{class_time}', 
									      sessionTeacher = '{session['TeacherName']}' 
									      where userID is '{self.username}' and
									      sessionDay is '{str(day.weekday())}' and
									      sessionID is '{str(sessionID)}'
						""")
		self.conn.commit()


	def add_cookies(self, update_cookies=False):
		"""
		Gets the users SIMON auth cookies converts them into morsel dicts
		so that they can be stored in the database and recreated has
		requests cookies objects later
		:return:
			A list containing the cookies in morsel form
		"""
		session = self.login()
		cookies = []
		jar = session.session.cookies
		for cookie in jar:
			morsel_dict = {
				'name': cookie.name,
				'value': cookie.value
			}
			self.session.cookies.set(**morsel_dict)
			cookies.append(morsel_dict)
		try:
			assert len(cookies) != 1, 'Need more cookies'
			self.db.execute(
				f"INSERT INTO tblCookies (userID, cookieSimonLogin) VALUES ('{self.username}', '{json.dumps(cookies)}') ")
		except Exception as e:
			print(e)
			print('cookies already in')
			if update_cookies is True and len(cookies) != 1:
				self.db.execute(f"UPDATE tblCookies Set cookieSimonLogin = '{json.dumps(cookies)}' WHERE userID = '{self.username}'")
		self.conn.commit()
		return cookies


	def get_cookies(self, bake_fresh=False):
		"""
		gets the cookies from the database
		:return:
			Requests cookie jar with SIMON cookies
		"""
		if bake_fresh:
			self.add_cookies(update_cookies=True)
		try:
			cookies = json.loads(self.db.execute(
				f"SELECT cookieSimonLogin FROM tblCookies WHERE userID is '{str(self.username)}'").fetchall()[
						     0][0])
		except (TypeError, IndexError):
			cookies = self.add_cookies()

		for cookie in cookies:
			self.session.cookies.set(**cookie)
		self.session.cookies.set('logondata', f'acc=0&Ign={self.username}')
		return self.session.cookies


	def get_timetable(self, day=datetime.now().date().isoformat()):
		"""
		Gets the timetable for the specified day
		:param day:
			The day in iso format that you want
		:return:
			The timetable in JSON format
		"""
		data = {'selectedDate': day, 'selectedGroup': None}
		return self.request_info('GetTimetable', require_cookies=True, data=json.dumps(data)).json()


	def get_guid(self, from_db=False):
		"""
		Gets the user's GUID from SIMON
		:return:
			User's GUID
		"""
		if from_db:
			return \
			self.db.execute(f"SELECT userGuid from tblUsers where userID is '{self.username}'").fetchall()[0][0]
		r = self.request_info('GetUserInfo')
		json_string = r.json()
		# Gets the GUID from a parameter of the users photo url
		guid = re.search(r"GUID=(.*)", json_string['d']['UserPhotoUrl']).group(1)
		return guid


	def get_sessions(self, day):
		"""
		Gets all the sessions for the day
		:param day:
			The day to you want to get data from1 - 5, 1 is Monday, 5 is Friday
		:return:
			generator which yields the session info session by session
		"""
		assert day in range(5), f'Invalid day specified. Must be between 0 and 4. {day} is not in that range'
		for session in self.db.execute(f"SELECT sessionClass, sessionRoom, sessionTime, sessionTeacher, sessionID from tblSession where sessionDay is {day} and userID is '{self.username}'"):
			yield session


	def get_session(self, day, session):
		"""
		Gets the data for that session of that day
		:param day:
			The day to you want to get data from
			1 - 5, 1 is Monday, 5 is Friday
		:param session:
			The session that you want
			1-9
		:return:
			List of the sessions
		"""
		assert day in range(6) and day != 0, 'Invalid day specified. Must be between 1 and 5'
		assert session in range(10) and session != 0, 'Invalid session specified. Must be between 1 and 9'
		info = self.db.execute(f"""select sessionClass, sessionRoom, sessionTime, sessionTeacher From tblSession
						join tblUsers tU on tblSession.userID = tU.userID where tU.userID is '{self.username}'
						and sessionDay is '{str(day)}' and sessionID is '{int(session)}'""").fetchall()[0]
		return info


	def get_session_cookie(self):
		"""
		Gets the session cookie from the database
		:return:
			Value of session cookie
		"""
		try:
			session_cookie = self.db.execute(
				f"""SELECT cookieSession FROM tblCookies WHERE userID is '{self.username}'""").fetchall()[0][0]
		except:
			return False
		return session_cookie


	def get_password_hash(self):
		"""
		Gets the password hash from the database
		:return:
			The password in pbkdf2_sha256 hash
		"""
		hash = self.db.execute(f"SELECT userPassword FROM tblUsers WHERE userID is '{self.username}'").fetchall()[0][0]
		return hash


	def get_average(self):
		data = json.dumps({"guidString": self.get_guid(from_db=True).upper()})
		semesterID = self.request_info(
			url="https://intranet.stpats.vic.edu.au/WebModules/Profiles/Student/LearningResources/LearningAreaTasks.aspx/getSemesters",
			data=data).json()['d'][0]['FileSeq']
		# semesterID = self.request_info(
		# 	url="https://intranet.stpats.vic.edu.au/WebModules/Profiles/Student/LearningResources/LearningAreaTasks.aspx/getSemesters",
		# 	data=data).text
		data = json.dumps({"guidString": self.get_guid(from_db=True), "fileSeq": semesterID})
		results = self.request_info(
			url="https://intranet.stpats.vic.edu.au/WebModules/Profiles/Student/LearningResources/LearningAreaTasks.aspx/getClasses",
			data=data).json()
		results_list = []
		for subject in results['d']['SubjectClasses']:
			for task in subject['Tasks']:
				try:
					log.debug(task['FinalResult'])
					result = re.search("(\d?\d?\d)+%", task['FinalResult']).group(1)
					log.debug(result)
					results_list.append(int(result))
				except:
					pass
		return round(sum(results_list) / len(results_list), 2)


	def get_calendar(self, start_date=datetime.now().date().isoformat().replace('-', '/'), end_date=(datetime.now().date() + timedelta(days=30)).isoformat()):
		"""
		Gets the calendar from SIMON
		:param start_date:
			The date in the format day/month/year of the start date
		:param end_date:
			The date in the format day/month/year of the end date
		:return:
			JSON of the students timetable
		"""
		data = json.dumps({
				'UID': None,
				'classCode': None,
				'inactive': None,
				'hasInactiveParameter': None,
				'startDate': start_date,
				'endDate': end_date})
		return self.request_info('GetSIMONCalendarEvents', data=data).json()


	def check_session_cookie(self, cookie):
		"""
		Checks if the cookie matches the users session cookie
		:param cookie:
			The cookie to check
		:return:
			Boolean of if the cookies match
		"""
		if self.get_session_cookie() == cookie:
			return True
		elif check_password is False:
			return True
		else:
			return False


	def check_password(self):
		"""
		Checks if the Student objects password matches the user's password
		:return:
			Boolean of if the passwords match
		"""
		if check_password is True:
			hash = self.get_password_hash()
			return pbkdf2_sha256.verify(self.password, hash)
		else:
			return True


	def check_cookies(self):
		"""
		Checks if the Student object has cookies set and if it doesn't then it sets them
		:return:
			Boolean of if the Student object had cookies set
		"""
		cookie_count = len(self.session.cookies)
		if cookie_count == 0:
			self.get_cookies()
			return False
		else:
			return True


	def is_real(self, add_user=True):
		"""
		Checks if the user is in the database
		:return:
			Boolean of if the user is in the database
		"""
		try:
			if self.db.execute(f"SELECT userID FROM tblUsers WHERE userID is {self.username}""").fetchall() != []:
				print('user in database')
				return True
			else:
				return False
		except:
			print('user not in database')
			try:
				if add_user:
					print('adding user')
					self.add_user()
				else:
					pass
			except:
				print('unable to add user')
				return False
			else:
				print('user added')
				return True


def update_students_day():
	"""
	updates all the timetables of the students in the DB
	:return:
		Nothing
	"""
	conn = sqlite3.connect('database.db')
	db = conn.cursor()
	# pool = Pool(cpu_count() - 1)
	student_list = db.execute("SELECT userID from tblUsers").fetchall()
	print(f'updating timetable for {len(student_list)} people')
	for student_num in student_list:
		student = Student(student_num[0])
		student.add_timetable_for_day()
