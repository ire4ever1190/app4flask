import os

def log_template(log_level):
	return lambda message: print(f"{log_level.upper()}: {message}")

class Logging:
	def __init__(self):
		log_level = os.getenv('log_levels')
		if log_level:
			self.log_levels = log_level.split(" ")
		else:
			self.log_levels = ["info"]
		for log_level in self.log_levels:
			setattr(self, log_level, log_template(log_level))

	def __getattr__(self, item):
		if item in self.log_levels:
			return getattr(self, item)
		else:
			return self.dummy

	def __dir__(self):
		return self.log_levels

	def dummy(self, *args):
		pass



