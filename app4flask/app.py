from datetime import datetime
import os
import Forms
import pytz
from config import Config
from datahandler import *
from flask import Flask, jsonify, make_response, redirect, render_template, request
from apscheduler.schedulers.background import BackgroundScheduler

scheduler = BackgroundScheduler()
app = Flask(__name__)
app.config.from_object(Config)
try:
	check_password_env = os.environ['check_password']
	if check_password_env == 'False':
		check_password = False
	else:
		check_password = True
except KeyError:
	check_password = True

# This returns a json list of the current day
# or if the day is passed in the url then it returns that
@app.route("/list/<today>", methods=["POST", "GET"])
@app.route("/list/", methods=["POST", "GET"])
@app.route("/list", methods=["POST", "GET"])
def show_info(today=None):
	"""
	The REST endpoint to respond with the timetable for the current day or a certain day
	:param today:
		Whether a certain day is asked for
	:return:
		JSON string containing the timetable for the day
	"""
	# Checks if the day has been specified then if it hasn't it just goes with\ system timezone
	# If the timezone was specified then it reads the header and uses that
	session_cookie = request.cookies.get('sessionCookie')
	userID = session_cookie[-5:] if session_cookie is not None else None
	if session_cookie is not None and check_password is True:
		userID = session_cookie[-5:]
		return "need session cookie"
	if check_password is False and request.args.get('username') is not None:
		userID = request.args.get('username')

	student = Student(userID)
	if student.check_session_cookie(session_cookie) is False:
		return "invalid session cookie"

	if today is None:
		if request.headers.get('timezone') is None:
			today = datetime.now().weekday()
		else:
			timezone = pytz.timezone(request.headers.get('timezone'))
			today = datetime.now(timezone).weekday()
	today = int(today)
	if today < 0 or today > 4:
		classes = [{'day': today}]
		for i in range(1, 10):
			session = {'Class': "Nothing", 'Room': "Nothing", 'Time': "Nothing", 'Teacher': "Nothing"}
			classes[0]["session" + str(i)] = session
		return jsonify(classes)


	if request.headers.get('update') == 'True' and request.method == 'POST':
		password = str(request.headers.get('password'))
		student.password = password
		student.add_data()
	elif request.headers.get('update') == 'True' and request.method == 'GET':
		return "Please make a POST request when updating and not a GET request"

	# try:

	classes = [{'day': today}]

	# Gets json from database and creates the JSON that will be returned
	student = Student(userID)
	print(student.username)
	for session in student.get_sessions(today):
		session_data = {'Class': session[0], 'Room': session[1], 'Time': session[2], 'Teacher': session[3]}
		classes[0]["session" + str(session[4])] = session_data
	return jsonify(classes)



def show_html(studentNum):
	"""
	just a shortcut to show the timetable template
	:param studentNum:
		The students number
	:return:
		The timetable HTML document
	"""
	return render_template('default.html', user=studentNum, )


@app.route("/login")
@app.route("/")
def index():
	"""
	Shows the login form
	:return:
		The login page
	"""
	session_cookie = request.cookies.get('sessionCookie')
	if session_cookie is not None:
		username = session_cookie[-5:]
		student = Student(username)
		if student.is_real():
			if student.check_session_cookie(session_cookie):
				return redirect('/timetable')
	form = Forms.LoginForm()
	return render_template("Login.html", form=form)

@app.route("/average", methods=["GET"])
def average():
	#TODO make checking of session cookie respond to the none checking of password
	userID = None
	session_cookie = request.cookies.get('sessionCookie')

	if session_cookie is None and check_password is True:
		userID = session_cookie[-5:]
		return "need session cookie"
	if check_password is False and request.args.get('username') is not None:
		userID = request.args.get('username')

	student = Student(userID)
	if student.check_session_cookie(session_cookie) is False:
		return "invalid session cookie"
	return str(student.get_average())


@app.route("/timetable", methods=["GET", "POST"])
def timetable():
	"""
	This is the route for the timetable page. This handles all the checking of the form
	and seeing if the person is remembered or if they need to have their timetable updated
	:return:
		The timetable page
	"""

	def make_cookie_response(student, max_age=60 * 60 * 24 * 92):
		"""
		Makes a response that sets the studentNum cookie and shows the timetable
		:param studentNum:
			The student number
		:return:
			timetable page and cookie with student number
		"""
		response = make_response(show_html(student.username))
		cookieValue = student.add_session_cookie()
		response.set_cookie('sessionCookie', cookieValue, max_age=max_age)
		return response

	session_cookie = request.cookies.get('sessionCookie')
	if request.method == "GET":
		if session_cookie is None:
			return redirect('/login')
		else:
			username = session_cookie[-5:]
			student = Student(username)
			if student.is_real() and student.check_session_cookie(session_cookie):
				return show_html(username)
			else:
				return redirect('/login')
	elif request.method == "POST":  # If the request is a form
		username = request.form.get('username')
		password = request.form.get('password')
		remember_me = request.form.get('remember')
		update = request.form.get('update')
		print(username, password)
		student = Student(username, password)
		student_real = student.is_real()
		print(student_real)
		if student_real is False:
			print('not in database')
			student.add_user()
			student.add_data()

		if student.check_password() is False:
			return redirect('/login')

		if update == 'y' and remember_me is None:
			student.add_data()
			return make_cookie_response(student, None)

		elif update is None and remember_me == 'y':
			return make_cookie_response(student)

		elif update == 'y' and remember_me == 'y':
			student.add_data()
			return make_cookie_response(student)

		else:
			return make_cookie_response(student, None)
	else:
		return 'use form POST requests or just browser GET requests'


# This make checks to see what buttons where clicked


@app.route("/sw.js")
def sw():
	"""
	Sends the service worker javascript file
	:return:
		sw.js file
	"""
	return app.send_static_file("sw.js")


if __name__ == "__main__":
	scheduler.add_job(update_students_day, 'cron', day_of_week='mon-fri', hour=5)
	scheduler.start()
	app.run()
